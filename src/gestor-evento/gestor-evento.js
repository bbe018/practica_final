import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import'@polymer/iron-ajax/iron-ajax.js'
import '../login-usuario/login-usuario.js';
import '../logout-usuario/logout-usuario.js';
import '../visor-cuentas/visor-cuentas.js';
import '../visor-usuario/visor-usuario.js';
import '../alta-usuario/alta-usuario.js';
/**
 * @customElement
 * @polymer
 */
class GestorEvento extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          color: black;
        }

        }


      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <login-usuario on-myevent="processEvent"></login-usuario>
      <span hidden$="{{!isDisplayed}}">   <visor-usuario id="receiver"></visor-usuario></span>



    `;
  }
  static get properties() {
    return {
      isDisplayed:{
        type: Boolean,
        value: false
      }
    };
  }// end properties

processEvent(e){
  console.log("Capturado evento emisor desde Gestor");
  console.log(e.detail);
  this.isDisplayed=e.detail.in;
  this.$.receiver.id = e.detail.id;
  this.$.receiver.displayed=e.detail.in;
}


}//end class

window.customElements.define('gestor-evento', GestorEvento);
