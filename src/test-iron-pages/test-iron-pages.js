import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import'@polymer/iron-pages/iron-pages.js'
import '../emisor-evento/emisor-evento.js';
import '../receptor-evento/receptor-evento.js';
/**
 * @customElement
 * @polymer
 */
class TestIronPages extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          color: black;
        }

        }


      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h1 class="jumbotron">Test-iron-pages</h1>

      <select value="{{componentName::change}}" placeholder="Seleccionar componente">
        <option value=" ">Seleccionar componente</option>
        <option value="visor">Visor cuentas</option>
        <option value="login">Login usuario</option>
      </select>
      <iron-pages selected="[[componentName]]" attr-for-selected="component-name">
        <div component-name="visor"><visor-cuentas id="22"></visor-cuentas></div>
        <div component-name="login"><login-usuario></login-usuario></div>
      </iron-pages>

    `;
  }
  static get properties() {
    return {
      componentName : {
        type: String
      }
    };
  }// end properties



}//end class

window.customElements.define('test-iron-pages', TestIronPages);
