import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import'@polymer/iron-ajax/iron-ajax.js'
/**
 * @customElement
 * @polymer
 */
class LogoutUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          color: black;
        }

        }


      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h2>Logout Usuario</h2>
      <button class="btn btn-danger" on-click="logout">Logout</button>
      <span hidden$="{{!isLoggedOut}}">Logout correcto</span>
      <h2></h2>
      <iframe hidden$="{{!isLoggedOut}}" src="https://www.foaas.com/bye/apitechu" width="1000" height="350" frameborder="no" Scrolling="no" >
      </iframe>
      <h2></h2>


      <iron-ajax id="doLogout"
      url="http://localhost:3000/apitechu/v2/logout/{{id}}"
      method="POST"
      on-response="manageAJAXResponse"
      on-error="showError">
      </iron-ajax>

    `;
  }
  static get properties() {
    return {
      isLoggedOut:{
        type: Boolean,
        value: false
      },
      id: {
        type: Number
      }
    };
  }// end properties

    logout(){
      console.log("Usuario ha pulsado botón");
      console.log("Voy a enviar la petición");
      console.log(this.id);
      this.$.doLogout.generateRequest();
      console.log("Petición enviada");
      window.location.assign("http://127.0.0.1:8081/components/frontproyecto/");
    }


  showError(error){
    console.log("Error");
    console.log(error);
  }

  manageAJAXResponse(){
    console.log("manageAJAXResponse");
    this.isLoggedOut=true;
    this.dispatchEvent(
      new CustomEvent(
        "myeventout", {
          "detail" : {
            "in" : false
          }
        }
      )
    )
  }
}//end class

window.customElements.define('logout-usuario', LogoutUsuario);
