import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import'@polymer/iron-ajax/iron-ajax.js'
/**
 * @customElement
 * @polymer
 */
class LoginUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          color: black;
        }

        }


      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h2>Login Usuario</h2>
      <input type="text" placeholder="email" value="{{email::input}}"></input>
      <input type="password" placeholder="password" value="{{password::input}}"></input>
      <button on-click="login" class="btn btn-success btn-lg">Login</button>
      <span hidden$="{{!isLogged}}">Login correcto</span>
      <h2></h2>
      <iframe hidden$="{{!isLogged}}" src="https://www.foaas.com/cool/[[email]]" width="1000" height="350" frameborder="no" Scrolling="no" >
      </iframe>
      <h2></h2>


      <iron-ajax id="doLogin"
      url="http://localhost:3000/apitechu/v2/login"
      handle-as="json"
      method="POST"
      content-type="application/json"
      on-response="manageAJAXResponse"
      on-error="showError">
      </iron-ajax>

    `;
  }
  static get properties() {
    return {
      password: {
        type: String
      },
      email: {
        type: String
      },
      isLogged:{
        type: Boolean,
        value: false
      }
    };
  }// end properties

    login(){
      console.log("Usuario ha pulsado botón");
      console.log("Voy a enviar la petición");
      var loginData={
        "email": this.email,
        "password":this.password
      }
      console.log(loginData);

      this.$.doLogin.body = JSON.stringify(loginData);
      this.$.doLogin.generateRequest();
      console.log("Petición enviada");
    }

  showData(data){
    console.log("showData");
    console.log(data.detail.response);
    this.first_name=data.detail.response.first_name
    this.last_name=data.detail.response.last_name
    this.email=data.detail.response.email
  }

  showError(error){
    console.log("Error");
    console.log(error);
  }

  manageAJAXResponse(data){
    console.log("manageAJAXResponse");
    console.log(data.detail.response);
    this.isLogged=true;
    console.log(data.detail.response);

    this.dispatchEvent(
      new CustomEvent(
        "myevent", {
          "detail" : {
            "id" : data.detail.response.id,
            "in" : true
          }
        }
      )
    )

  }
}//end class

window.customElements.define('login-usuario', LoginUsuario);
