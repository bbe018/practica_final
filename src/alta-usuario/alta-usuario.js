import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import'@polymer/iron-ajax/iron-ajax.js'
/**
 * @customElement
 * @polymer
 */
class AltaUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          color: black;
        }

        }


      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h2>Alta Usuario</h2>
      <h4>Id usuario</h4>
      <input type="range" value={{id::input}} min=100 max=999999 required></input>
      <h4>Nombre</h4>
      <input type="text" placeholder="First name" value="{{first_name::input}}" required></input>
      <h4>Primer apellido</h4>
      <input type="text" placeholder="Last name" value="{{last_name::input}}" required></input>
      <h4>Correo electrónico</h4>
      <input type="text" placeholder="email" value="{{email::input}}" required></input>
      <h4>Contraseña</h4>
      <input type="password" placeholder="password" value="{{password::input}}" required></input>
      <h4></h4>
      <button class="btn btn-success btn-lg" on-click="alta">Alta</button>
      <span hidden$="{{!isLogged}}">Alta correcta</span>
      <h2></h2>
      <iframe hidden$="{{!isLogged}}" src="https://www.foaas.com/bm/[[first_name]] [[last_name]]/APITECHU" width="1000" height="350" frameborder="no" Scrolling="no" >
      </iframe>
      <h2></h2>


      <iron-ajax id="CreateUser"
      url="http://localhost:3000/apitechu/v2/users"
      handle-as="json"
      method="POST"
      content-type="application/json"
      on-response="manageAJAXResponse"
      on-error="showError">
      </iron-ajax>

    `;
  }
  static get properties() {
    return {
      password: {
        type: String
      },
      email: {
        type: String
      },
      id: {
        type: Number
      },
      first_name: {
        type: String
      },
      last_name: {
        type: String
      },
      isLogged:{
        type: Boolean,
        value: false
      }
    };
  }// end properties

    alta(){
      console.log("Usuario ha pulsado botón");
      console.log("Voy a enviar la petición");
      var loginData={
        "email": this.email,
        "password":this.password,
        "first_name": this.first_name,
        "last_name": this.last_name,
        "id": parseInt(this.id)
      }
      console.log(loginData);

      this.$.CreateUser.body = JSON.stringify(loginData);
      this.$.CreateUser.generateRequest();
      console.log("Petición enviada");
    }

  showData(data){
    console.log("showData");
    console.log(data.detail.response);
    this.first_name=data.detail.response.first_name
    this.last_name=data.detail.response.last_name
    this.email=data.detail.response.email
  }

  showError(error){
    console.log("Error");
    console.log(error);
  }

  manageAJAXResponse(data){
    console.log("manageAJAXResponse");
    console.log(data.detail.response);
    this.isLogged=true;
    console.log(data.detail.response);

    this.dispatchEvent(
      new CustomEvent(
        "myevent", {
          "detail" : {
            "id" : data.detail.response.id
          }
        }
      )
    )

  }
}//end class

window.customElements.define('alta-usuario', AltaUsuario);
