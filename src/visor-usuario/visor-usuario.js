import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import'@polymer/iron-ajax/iron-ajax.js'
import '../visor-cuentas/visor-cuentas.js';
/**
 * @customElement
 * @polymer
 */
class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          color: black;
        }

        }


      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">



      <span hidden$="{{!displayed}}">
       <button on-click="sendEvent">Recuperar cuentas cliente:</button>
       <visor-cuentas id="cuentas"></visor-cuentas>
      </span>
      
      <logout-usuario id="logout"></logout-usuario>




      <iron-ajax id="getUser"
      url="http://localhost:3000/apitechu/v2/users/{{id}}"
      handle-as="json"
      on-response="showData">
      </iron-ajax>

    `;
  }
  static get properties() {
    return {
      first_name: {
        type: String
      },
      id: {
        type: Number,
        observer : "_idChanged"
      },
      last_name: {
        type: String
      },
      displayed:{
        type: Boolean,
        value: false
      }
    };
  }// end properties

  showData(data){
    console.log("showData");
    console.log(data.detail.response);
    this.first_name=data.detail.response.first_name
    this.last_name=data.detail.response.last_name
  }

  _idChanged(newValue,oldValue){

    console.log("id has changed");
    console.log("Old value " + oldValue);
    console.log("New value "+ newValue);
    if (newValue > 0 )
    {
    this.$.getUser.generateRequest();
    }

  }
  sendEvent(e){
    console.log("El usuario ha pulsado el botón");
    console.log(e)
    this.$.cuentas.id = this.id;
    this.$.logout.id=this.id;

  }
}//end class

window.customElements.define('visor-usuario', VisorUsuario);
