import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import'@polymer/iron-ajax/iron-ajax.js'
/**
 * @customElement
 * @polymer
 */
class VisorCuentas extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          color: black;
        }

        }


      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <h2></h2>
      <dom-repeat items="{{accounts}}">
      <template>
        <h2>IBAN - {{item.IBAN}}</h2>
        <h2>Saldo - {{item.saldo}}</h2>
        <a href="http://127.0.0.1:8081/components/frontproyecto/alta-movimientos.html?id={{item.IBAN}}&s={{item.saldo}}">Nuevo movimiento</a>
        <a href="http://127.0.0.1:8081/components/frontproyecto/visor-movimientos.html?id={{item.IBAN}}&s={{item.saldo}}" target="_blank">Consultar movimientos</a>
        </template>
      </dom-repeat>

      <h2></h2>
      <iron-ajax id="getAccounts"
      url="http://localhost:3000/apitechu/v1/accounts/{{id}}"
      handle-as="json"
      on-response="showData">
      </iron-ajax>


    `;
  }
  static get properties() {
    return {
      id: {
        type: Number,
        observer : "_idChanged"
      },
      accounts: {
        type: Array
      }
    };
  }// end properties

  showData(data){
    console.log("showData");
    console.log(data.detail.response);
    this.accounts=data.detail.response;

  }

  _idChanged(newValue,oldValue){

    console.log("id has changed");
    console.log("Old value " + oldValue);
    console.log("New value "+ newValue);
    if (newValue > 0 )
    {
    this.$.getAccounts.generateRequest();
    }
  }
}//end class

window.customElements.define('visor-cuentas', VisorCuentas);
